import { Component,ViewChild, ElementRef } from '@angular/core';
import { NavController,NavParams,ToastController,IonicPage,AlertController} from 'ionic-angular';
import{FirebaseListObservable}from'angularfire2/database';
import{AngularFireDatabase}from'angularfire2/database';
import{AngularFireModule}from'angularfire2';

import firebase from 'firebase';

import{MapsPage}from'../maps/maps';
import{RoutesPage}from'../routes/routes';



@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})


export class HomePage {
  

public from:any;


public details:any;



 public trainList: Array<any>;
 public trainnameList:Array<any>;
 
public userProfileRef:firebase.database.Reference;
public ref:firebase.database.Reference;

public tos:firebase.database.Reference;
public dates:firebase.database.Reference;
public datesList:Array<any>;

public tolist:Array<any>;

public tr:any;

  constructor(public navCtrl: NavController,
	public toastCtrl:ToastController,
  public navParams:NavParams,
	public af: AngularFireDatabase,

  public alertCtrl:AlertController


	) {
	
   this.userProfileRef = firebase.database().ref('/');
   

//this.getAccountList();
    


  }
	 getfromLists():firebase.database.Reference {

    return this.userProfileRef;
  }

/*searchtrainno()
{

  this.userProfileRef.startAt('BHUBANESWAR')
    .endAt('BHUBANESWAR')
    .once('value', this.show);

}
show(snap) {
  
   console.log("Value="+snap.val().trainName);


}*/


 showtime():void{
    let prompt=this.alertCtrl.create({
      title:'Arrival time',
      message:'Enter the arrival time within single quotes',
      inputs:[
        {
          name:'Start',
          placeholder:'Start'
        },
         {
          name:'End',
          placeholder:'End'
        }
      ],
      buttons:[
        {
          text:"ok",
          handler:data=>
          {
            this.searcharrival(data.Start,data.End);
          }
        },
        {
          text:"Cancel",
          handler:data=>
          {
            console.log("cancel clicked");
          }
         

            
          }
        
           
      ]
    });
    prompt.present();
  }





searcharrival(Start,End)
{
 var refe = firebase.database().ref("/");
 
 //refe.orderByChild("Arrivaltime").startAt(Start).endAt(End).on("child_added",snapshot=> {

this.tolist=[];
 refe.orderByChild("Arrivaltime").startAt(Start).endAt(End).on("child_added",snapshot=> {
 
    this.tolist.push({
          id: snapshot.key,
          Arrivaltime:snapshot.val().Arrivaltime,
          Departuretime:snapshot.val().Departuretime,
          DestinationStationName:snapshot.val().DestinationStationName,
          DestinationstationCode:snapshot.val().DestinationstationCode,
          Distance:snapshot.val().Distance,
          islno:snapshot.val().islno,
          sourceStationName:snapshot.val().sourceStationName,
          SourceStationCode:snapshot.val().SourceStationCode,
          StationName:snapshot.val().StationName,
          stationCode:snapshot.val().stationCode,
          trainName:snapshot.val().trainName,
          TrainNo:snapshot.val().TrainNo


    
        });
        return false;

 })
         console.log(this.tolist);
               
     //console.log(snapshot.key, snapshot.val().Arrivaltime,snapshot.val().TrainNo,snapshot.val().Departuretime,snapshot.val().DestinationstationCode,
  //snapshot.val().DestinationStationName);
          
}
   
  
 searchbytrainno():void{
    let prompt=this.alertCtrl.create({
      title:'Enter the train no',
      message:'Enter the Train no within single quotes',
      inputs:[
        {
          name:'TrainNo',
          placeholder:'TrainNo'
        }
      ],
      buttons:[
        {
          text:"ok",
          handler:data=>
          {
            this.searchtrainno(data.TrainNo);
          }
        },
        {
          text:"Cancel",
          handler:data=>
          {
            console.log("cancel clicked");
          }
         

            
          }
        
           
      ]
    });
    prompt.present();
  }
  

  searchtrainno(trainno)
  {
var refe = firebase.database().ref("/");
 

    this.tolist=[];
 refe.orderByChild("TrainNo").equalTo(trainno).on("child_added",snapshot=> {
           
 
    this.tolist.push({
          id: snapshot.key,
          Arrivaltime:snapshot.val().Arrivaltime,
          Departuretime:snapshot.val().Departuretime,
          DestinationStationName:snapshot.val().DestinationStationName,
          DestinationstationCode:snapshot.val().DestinationstationCode,
          Distance:snapshot.val().Distance,
          islno:snapshot.val().islno,
          sourceStationName:snapshot.val().sourceStationName,
          SourceStationCode:snapshot.val().SourceStationCode,
          StationName:snapshot.val().StationName,
          stationCode:snapshot.val().stationCode,
          trainName:snapshot.val().trainName,
          TrainNo:snapshot.val().TrainNo


    
        });
                 console.log(this.tolist);
         return false;
        

 })
  
 
        
          

  }









showmap()
{
  this.navCtrl.push(MapsPage);
}

showroute()
{
  this.navCtrl.push(RoutesPage);
}
 showdata()
   {
       this.getfromLists().on('value', snapshot => {
      this.tolist = [];
      snapshot.forEach( snap => {
        this.tolist.push({
          id: snap.key,
          Arrivaltime:snap.val().Arrivaltime,
          Departuretime:snap.val().Departuretime,
          DestinationStationName:snap.val().DestinationStationName,
          DestinationstationCode:snap.val().DestinationstationCode,
          Distance:snap.val().Distance,
          islno:snap.val().islno,
          sourceStationName:snap.val().sourceStationName,
          SourceStationCode:snap.val().SourceStationCode,
          StationName:snap.val().StationName,
          stationCode:snap.val().stationCode,
          trainName:snap.val().trainName,
          TrainNo:snap.val().TrainNo


        }); 
     
console.log(this.trainList);
    
        return false
      });

       
 
 
    });   
 
   
   }


 
 
    
}



 
		// console.log("State change => "+JSON.stringify(this.dis
